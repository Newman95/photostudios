using System;
using System.Linq.Expressions;
using Photostudios.Domain;

namespace Photostudios.Cqrs.Queries.Cities.Models
{
    public class CityDetailDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public static Expression<Func<City, CityDetailDto>> Projection =>
            c => new CityDetailDto
            {
                Id = c.Id,
                Name = c.Name
            };
    }
}
