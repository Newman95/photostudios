using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Photostudios.Cqrs.Queries.Cities.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Cities.GetCitiesList
{
    public class GetCitiesListQueryHandler : IRequestHandler<GetCitiesListQuery, List<CityDetailDto>>
    {
        private readonly AppDbContext _context;

        public GetCitiesListQueryHandler(AppDbContext context)
        {
            _context = context;
        }

        public Task<List<CityDetailDto>> Handle(GetCitiesListQuery request, CancellationToken cancellationToken)
        {
            var cities = _context.Cities.AsNoTracking().AsQueryable();

            if (request.Skip > 0)
            {
                cities = cities.Skip(request.Skip);
            }

            if (request.Top > 0)
            {
                cities = cities.Take(request.Top);
            }

            return cities.Select(CityDetailDto.Projection).ToListAsync(cancellationToken);
        }
    }
}
