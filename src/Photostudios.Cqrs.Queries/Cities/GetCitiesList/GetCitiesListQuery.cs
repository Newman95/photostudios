using System.Collections.Generic;
using MediatR;
using Photostudios.Cqrs.Queries.Cities.Models;

namespace Photostudios.Cqrs.Queries.Cities.GetCitiesList
{
    public class GetCitiesListQuery : IRequest<List<CityDetailDto>>
    {
        public int Skip { get; set; }

        public int Top { get; set; }
    }
}
