using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.Cqrs.Queries.Cities.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Cities.GetCityDetailById
{
    public class GetCityDetailByIdQueryHandler : IRequestHandler<GetCityDetailByIdQuery, CityDetailDto>
    {
        private readonly AppDbContext _context;

        public GetCityDetailByIdQueryHandler(AppDbContext context)
        {
            _context = context;
        }

        public Task<CityDetailDto> Handle(GetCityDetailByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_context.Cities
                .Where(c => c.Id == request.Id)
                .Select(CityDetailDto.Projection)
                .SingleOrDefault());
        }
    }
}
