using MediatR;
using Photostudios.Cqrs.Queries.Cities.Models;

namespace Photostudios.Cqrs.Queries.Cities.GetCityDetailById
{
    public class GetCityDetailByIdQuery : IRequest<CityDetailDto>
    {
        public int Id { get; set; }
    }
}
