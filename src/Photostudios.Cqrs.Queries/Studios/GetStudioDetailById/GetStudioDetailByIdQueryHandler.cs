using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.Cqrs.Queries.Studios.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Studios.GetStudioDetailById
{
    public class GetStudioDetailByIdQueryHandler : IRequestHandler<GetStudioDetailByIdQuery, StudioDetailDto>
    {
        private readonly AppDbContext _context;

        public GetStudioDetailByIdQueryHandler(AppDbContext context)
        {
            _context = context;
        }
        public Task<StudioDetailDto> Handle(GetStudioDetailByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_context.Studios
                .Where(c => c.Id == request.Id)
                .Select(StudioDetailDto.Projection)
                .SingleOrDefault());
        }
    }
}
