using MediatR;
using Photostudios.Cqrs.Queries.Studios.Models;

namespace Photostudios.Cqrs.Queries.Studios.GetStudioDetailById
{
    public class GetStudioDetailByIdQuery : IRequest<StudioDetailDto>
    {
        public int Id { get; set; }
    }
}
