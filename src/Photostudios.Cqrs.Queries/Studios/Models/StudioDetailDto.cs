using System;
using System.Linq.Expressions;
using Photostudios.Domain;

namespace Photostudios.Cqrs.Queries.Studios.Models
{
    public class StudioDetailDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int LogoId { get; set; }

        public Guid OwnerId { get; set; }

        public int CityId { get; set; }

        public static Expression<Func<Studio, StudioDetailDto>> Projection =>
            s => new StudioDetailDto
            {
                Id = s.Id,
                Name = s.Name,
                Address = s.Address,
                LogoId = s.LogoId,
                OwnerId = s.OwnerId,
                CityId = s.CityId
            };
    }
}
