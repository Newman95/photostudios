using System.Collections.Generic;
using MediatR;
using Photostudios.Cqrs.Queries.Studios.Models;

namespace Photostudios.Cqrs.Queries.Studios.GetStudiosList
{
    public class GetStudiosListQuery : IRequest<List<StudioDetailDto>>
    {
        public int Skip { get; set; }

        public int Top { get; set; }
    }
}
