using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Photostudios.Cqrs.Queries.Studios.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Studios.GetStudiosList
{
    public class GetStudiosListQueryHandler : IRequestHandler<GetStudiosListQuery, List<StudioDetailDto>>
    {
        private readonly AppDbContext _context;

        public GetStudiosListQueryHandler(AppDbContext context)
        {
            _context = context;
        }

        public Task<List<StudioDetailDto>> Handle(GetStudiosListQuery request, CancellationToken cancellationToken)
        {
            var cities = _context.Studios.AsNoTracking().AsQueryable();

            if (request.Skip > 0)
            {
                cities = cities.Skip(request.Skip);
            }

            if (request.Top > 0)
            {
                cities = cities.Take(request.Top);
            }

            return cities.Select(StudioDetailDto.Projection).ToListAsync(cancellationToken);
        }
    }
}
