using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Photostudios.Cqrs.Queries.Users.Models;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Users.GetUsersList
{
    public class GetUsersListQueryHandler : IRequestHandler<GetUsersListQuery, List<UserDetailDto>>
    {
        private readonly UserManager<User> _userManager;

        public GetUsersListQueryHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public Task<List<UserDetailDto>> Handle(GetUsersListQuery request, CancellationToken cancellationToken)
        {
            var users = _userManager.Users.AsNoTracking().AsQueryable();

            if (request.Skip > 0)
            {
                users = users.Skip(request.Skip);
            }

            if (request.Top > 0)
            {
                users = users.Take(request.Top);
            }

            return users.Select(UserDetailDto.Projection).ToListAsync(cancellationToken);
        }
    }
}
