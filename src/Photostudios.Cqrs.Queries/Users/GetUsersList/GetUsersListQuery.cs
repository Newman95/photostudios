using System.Collections.Generic;
using MediatR;
using Photostudios.Cqrs.Queries.Users.Models;

namespace Photostudios.Cqrs.Queries.Users.GetUsersList
{
    public class GetUsersListQuery : IRequest<List<UserDetailDto>>
    {
        public int Skip { get; set; }

        public int Top { get; set; }
    }
}
