using System;
using MediatR;
using Photostudios.Cqrs.Queries.Users.Models;

namespace Photostudios.Cqrs.Queries.Users.GetUserById
{
    public class GetUserByIdQuery : IRequest<UserDetailDto>
    {
        public Guid Id { get; set; }
    }
}
