using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Users.Models
{
    public class UserDetailDto
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public IEnumerable<string> Roles { get; set; }

        public static Expression<Func<User, UserDetailDto>> Projection =>
            u => new UserDetailDto
            {
                Id = u.Id,
                Email = u.Email,
                UserName = u.UserName,
                Roles = u.UserRoles.Select(ur => ur.Role.Name)
            };
    }
}
