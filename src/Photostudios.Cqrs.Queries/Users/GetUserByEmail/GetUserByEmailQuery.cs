using MediatR;
using Photostudios.Cqrs.Queries.Users.Models;

namespace Photostudios.Cqrs.Queries.Users.GetUserByEmail
{
    public class GetUserByEmailQuery : IRequest<UserDetailDto>
    {
        public string Email { get; set; }
    }
}
