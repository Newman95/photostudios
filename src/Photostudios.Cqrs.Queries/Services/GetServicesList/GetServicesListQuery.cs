using System.Collections.Generic;
using MediatR;
using Photostudios.Cqrs.Queries.Services.Models;

namespace Photostudios.Cqrs.Queries.Services.GetServicesList
{
    public class GetServicesListQuery : IRequest<List<ServiceDetailDto>>
    {
        public int Skip { get; set; }

        public int Top { get; set; }
    }
}
