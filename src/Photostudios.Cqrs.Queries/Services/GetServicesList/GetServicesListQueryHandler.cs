using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Photostudios.Cqrs.Queries.Services.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Services.GetServicesList
{
    public class GetServicesListQueryHandler : IRequestHandler<GetServicesListQuery, List<ServiceDetailDto>>
    {
        private readonly AppDbContext _context;

        public GetServicesListQueryHandler(AppDbContext context)
        {
            _context = context;
        }

        public Task<List<ServiceDetailDto>> Handle(GetServicesListQuery request, CancellationToken cancellationToken)
        {
            var cities = _context.Services.AsNoTracking().AsQueryable();

            if (request.Skip > 0)
            {
                cities = cities.Skip(request.Skip);
            }

            if (request.Top > 0)
            {
                cities = cities.Take(request.Top);
            }

            return cities.Select(ServiceDetailDto.Projection).ToListAsync(cancellationToken);
        }
    }
}
