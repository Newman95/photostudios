using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.Cqrs.Queries.Services.Models;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Queries.Services.GetServiceDetailById
{
    public class GetServiceDetailByIdQueryHandler : IRequestHandler<GetServiceDetailByIdQuery, ServiceDetailDto>
    {
        private readonly AppDbContext _context;

        public GetServiceDetailByIdQueryHandler(AppDbContext context)
        {
            _context = context;
        }

        public Task<ServiceDetailDto> Handle(GetServiceDetailByIdQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_context.Services
                .Where(c => c.Id == request.Id)
                .Select(ServiceDetailDto.Projection)
                .SingleOrDefault());
        }
    }
}
