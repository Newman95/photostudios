using MediatR;
using Photostudios.Cqrs.Queries.Services.Models;

namespace Photostudios.Cqrs.Queries.Services.GetServiceDetailById
{
    public class GetServiceDetailByIdQuery : IRequest<ServiceDetailDto>
    {
        public int Id { get; set; }
    }
}
