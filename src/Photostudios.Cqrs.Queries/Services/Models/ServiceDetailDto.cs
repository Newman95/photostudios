using Photostudios.Domain;
using System;
using System.Linq.Expressions;

namespace Photostudios.Cqrs.Queries.Services.Models
{
    public class ServiceDetailDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public static Expression<Func<Service, ServiceDetailDto>> Projection =>
            s => new ServiceDetailDto
            {
                Id = s.Id,
                Name = s.Name
            };
    }
}
