using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Photostudios.Cqrs.Queries.Roles.Models;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Roles.GetRolesList
{
    public class GetRolesListQueryHandler : IRequestHandler<GetRolesListQuery, List<RoleDetailDto>>
    {
        private readonly RoleManager<Role> _roleManager;

        public GetRolesListQueryHandler(
            RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public Task<List<RoleDetailDto>> Handle(GetRolesListQuery request, CancellationToken cancellationToken)
        {
            var roles = _roleManager.Roles.AsNoTracking().AsQueryable();

            if (request.Skip > 0)
            {
                roles = roles.Skip(request.Skip);
            }

            if (request.Top > 0)
            {
                roles = roles.Take(request.Top);
            }

            return roles.Select(RoleDetailDto.Projection).ToListAsync(cancellationToken);
        }
    }
}
