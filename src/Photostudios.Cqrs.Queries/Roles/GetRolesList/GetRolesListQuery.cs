using System.Collections.Generic;
using MediatR;
using Photostudios.Cqrs.Queries.Roles.Models;

namespace Photostudios.Cqrs.Queries.Roles.GetRolesList
{
    public class GetRolesListQuery : IRequest<List<RoleDetailDto>>
    {
        public int Skip { get; set; }

        public int Top { get; set; }
    }
}
