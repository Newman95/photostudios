using System;
using System.Linq.Expressions;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Roles.Models
{
    public class RoleDetailDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public static Expression<Func<Role, RoleDetailDto>> Projection =>
            r => new RoleDetailDto
            {
                Id = r.Id,
                Name = r.Name
            };
    }
}
