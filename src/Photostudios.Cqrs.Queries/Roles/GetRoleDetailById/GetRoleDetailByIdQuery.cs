using System;
using MediatR;
using Photostudios.Cqrs.Queries.Roles.Models;

namespace Photostudios.Cqrs.Queries.Roles.GetRoleDetailById
{
    public class GetRoleDetailByIdQuery : IRequest<RoleDetailDto>
    {
        public Guid Id { get; set; }
    }
}
