using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Cqrs.Queries.Roles.Models;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Roles.GetRoleDetailById
{
    public class GetRoleDetailByIdQueryHandler : IRequestHandler<GetRoleDetailByIdQuery, RoleDetailDto>
    {
        private readonly RoleManager<Role> _roleManager;

        public GetRoleDetailByIdQueryHandler(
            RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<RoleDetailDto> Handle(GetRoleDetailByIdQuery request, CancellationToken cancellationToken)
        {
            var role = await _roleManager.FindByIdAsync(request.Id.ToString());
            return new RoleDetailDto
            {
                Id = role.Id,
                Name = role.Name
            };
        }
    }
}
