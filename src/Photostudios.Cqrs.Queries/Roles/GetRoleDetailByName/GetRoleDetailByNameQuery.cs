using MediatR;
using Photostudios.Cqrs.Queries.Roles.Models;

namespace Photostudios.Cqrs.Queries.Roles.GetRoleDetailByName
{
    public class GetRoleDetailByNameQuery : IRequest<RoleDetailDto>
    {
        public string Name { get; set; }
    }
}
