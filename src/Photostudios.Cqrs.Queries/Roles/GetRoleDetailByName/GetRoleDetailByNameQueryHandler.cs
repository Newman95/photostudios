using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Cqrs.Queries.Roles.Models;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Queries.Roles.GetRoleDetailByName
{
    public class GetRoleDetailByNameQueryHandler
        : IRequestHandler<GetRoleDetailByNameQuery, RoleDetailDto>
    {
        private readonly RoleManager<Role> _roleManager;

        public GetRoleDetailByNameQueryHandler(
            RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<RoleDetailDto> Handle(GetRoleDetailByNameQuery request, CancellationToken cancellationToken)
        {
            var role = await _roleManager.FindByNameAsync(request.Name);
            return new RoleDetailDto
            {
                Id = role.Id,
                Name = role.Name
            };
        }
    }
}
