﻿namespace Photostudios.DataAccess.Abstractions
{
    public interface IEntity<out TKey>
    {
        TKey Id { get; }
    }
}
