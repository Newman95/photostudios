﻿using Microsoft.AspNetCore.Builder;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Photostudios.Web.Extensions.AppBuilder
{
    public static class SwaggerExtensions
    {
        public static IApplicationBuilder SetupSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(SetSwaggerOptions);

            return app;
        }

        private static void SetSwaggerOptions(SwaggerUIOptions options)
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "Photostudios API");
            options.RoutePrefix = "api/docs";
        }
    }
}