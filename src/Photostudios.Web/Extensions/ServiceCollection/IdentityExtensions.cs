using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Photostudios.DataAccess.EF;
using Photostudios.Domain.Identity;

namespace Photostudios.Web.Extensions.ServiceCollection
{
    public static class IdentityExtensions
    {
        public static IServiceCollection AddCustomizedIdentity(
            this IServiceCollection services)
        {
            services.AddIdentityCore<User>(SetIdentityOptions)
                    .AddRoles<Role>()
                    .AddRoleManager<RoleManager<Role>>()
                    .AddEntityFrameworkStores<AppDbContext>()
                    .AddDefaultTokenProviders();

            return services;
        }

        private static void SetIdentityOptions(IdentityOptions options)
        {
            // user
            options.User.RequireUniqueEmail = true;

            // password
            options.Password.RequireDigit = true;
            options.Password.RequiredLength = 6;
            options.Password.RequiredUniqueChars = 1;
            options.Password.RequireLowercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;

            // sign in
            options.SignIn.RequireConfirmedEmail = false;
            options.SignIn.RequireConfirmedPhoneNumber = false;
        }
    }
}