using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Photostudios.Configuration;
using Photostudios.Configuration.Abstractions;
using Photostudios.Configuration.Identity;
using System;
using System.Text;

namespace Photostudios.Web.Extensions.ServiceCollection
{
    public static class JwtAuthenticationExtensions
    {
        private static ITokenConfiguration _config = new TokenConfiguration();

        public static IServiceCollection AddCustomizedJwtAuthentication(
            this IServiceCollection services,
            IConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            config.Bind(ConfigurationPaths.Token, _config);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerOptions);

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
            });

            return services;
        }

        private static void JwtBearerOptions(JwtBearerOptions options)
        {
            options.SaveToken = false;

            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = false,

                ValidateAudience = true,
                ValidAudience = _config.Audience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Key)),

                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromDays(1)
            };
        }
    }
}