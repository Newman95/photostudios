﻿using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage("Reliability", "CA2007:Do not directly await a Task",
    Justification = "<Pending>",
    Scope = "Module")]

[assembly:
    SuppressMessage("Performance", "CA1822:Mark members as static",
    Justification = "<Pending>",
    Scope = "member",
    Target = "~M:Photostudios.Web.Startup.Configure(Microsoft.AspNetCore.Builder.IApplicationBuilder,Microsoft.AspNetCore.Hosting.IHostingEnvironment)")]

[assembly:
    SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded",
    Justification = "<Pending>",
    Scope = "member",
    Target = "~F:Photostudios.Web.Extensions.AppBuilder.ExceptionHandlerExtensions.ErrorHandlingPath")]

[assembly:
    SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded",
    Justification = "<Pending>",
    Scope = "member",
    Target = "~M:Photostudios.Web.Extensions.AppBuilder.SwaggerExtensions.SetSwaggerOptions(Swashbuckle.AspNetCore.SwaggerUI.SwaggerUIOptions)")]