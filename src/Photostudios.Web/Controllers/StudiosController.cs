﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Cqrs.Commands.Studios.CreateStudio;
using Photostudios.Cqrs.Commands.Studios.UpdateStudioById;
using Photostudios.Cqrs.Queries.Studios.GetStudioDetailById;
using Photostudios.Cqrs.Queries.Studios.GetStudiosList;
using Photostudios.Cqrs.Queries.Studios.Models;
using Photostudios.Dto.Studio;
using Photostudios.Web.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Photostudios.Mapping.Abstractions;
using MediatR;

namespace Photostudios.Web.Controllers
{
    [Route("api/studios")]
    [Authorize(Roles = "Admin")]
    public class StudiosController : ApiControllerBase
    {
        public StudiosController(IMapper mapper, IMediator mediator)
            : base(mapper, mediator)
        {
        }

        [HttpGet("{studioId:int}")]
        public async Task<ActionResult<StudioDetailDto>> GetStudioById(
            [FromRoute] int studioId)
        {
            return Ok(await Mediator.Send(new GetStudioDetailByIdQuery { Id = studioId }));
        }

        [HttpGet]
        public async Task<ActionResult<List<StudioDetailDto>>> GetStudios(
            [FromQuery] int skip,
            [FromQuery] int top)
        {
            return Ok(await Mediator.Send(new GetStudiosListQuery { Skip = skip, Top = top }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudio(
            [FromBody] CreateStudioDto createStudio)
        {
            await Mediator.Send(new CreateStudioCommand
            {
                Address = createStudio.Address,
                CityId = createStudio.CityId,
                LogoId = createStudio.LogoId,
                Name = createStudio.Name,
                OwnerId = createStudio.OwnerId,
                Phones = createStudio.Phones,
                Photos = createStudio.Photos,
                Services = createStudio.Services
            });

            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut("{studioId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateStudioById(
            [FromRoute] int studioId,
            [FromBody] UpdateStudioDto updateStudio)
        {
            await Mediator.Send(new UpdateStudioByIdCommand
            {
                Id = studioId,
                Address = updateStudio.Address,
                Name = updateStudio.Name
            });

            return Ok();
        }
    }
}