using MediatR;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Mapping.Abstractions;

namespace Photostudios.Web.Controllers.Base
{
    [ApiController]
    [Produces("application/json")]
    public abstract class ApiControllerBase : ControllerBase
    {
        protected ApiControllerBase(IMapper mapper, IMediator mediator)
        {
            Mapper = mapper;
            Mediator = mediator;
        }

        public IMapper Mapper { get; }
        public IMediator Mediator { get; }
    }
}