﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Photostudios.Configuration.Abstractions;
using Photostudios.Domain.Identity;
using Photostudios.Dto.Token;
using Photostudios.Dto.User;
using Photostudios.Mapping.Abstractions;
using Photostudios.Web.Controllers.Base;
using Swashbuckle.AspNetCore.Annotations;

namespace Photostudios.Web.Controllers
{
    [Route("api")]
    [AllowAnonymous]
    public class TokensController : ApiControllerBase
    {
        protected UserManager<User> UserManager { get; }
        protected ITokenConfiguration TokenConfig { get; }

        public TokensController(
            IMapper mapper,
            IMediator mediator,
            UserManager<User> userManager,
            ITokenConfiguration tokenConfig)
            : base(mapper, mediator)
        {
            UserManager = userManager;
            TokenConfig = tokenConfig;
        }

        /// <summary>
        /// Get auth token
        /// </summary>
        [HttpPost]
        [Route("token")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(TokenDto), description: "Successful")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(string), description: "User not found")]
        public async Task<IActionResult> Token([FromBody]LoginUserDto model)
        {
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var userRoles = await UserManager.GetRolesAsync(user);
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            foreach (var role in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenConfig.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                null,
                TokenConfig.Audience,
                claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: creds);

            var result = new TokenDto
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            return Ok(result);
        }
    }
}