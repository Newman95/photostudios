﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Cqrs.Commands.Cities.CreateCity;
using Photostudios.Cqrs.Commands.Cities.DeleteCityById;
using Photostudios.Cqrs.Commands.Cities.UpdateCityById;
using Photostudios.Cqrs.Queries.Cities.GetCitiesList;
using Photostudios.Cqrs.Queries.Cities.GetCityDetailById;
using Photostudios.Cqrs.Queries.Cities.Models;
using Photostudios.Dto.City;
using Photostudios.Web.Controllers.Base;
using Microsoft.AspNetCore.Http;
using MediatR;
using Photostudios.Mapping.Abstractions;

namespace Photostudios.Web.Controllers
{
    [Route("api/cities")]
    //[Authorize(Roles = "Admin")]
    public class CitiesController : ApiControllerBase
    {
        public CitiesController(IMapper mapper, IMediator mediator)
            : base(mapper, mediator)
        {
        }

        [HttpGet("{cityId:int}")]
        public async Task<ActionResult<CityDetailDto>> GetCityById(
            [FromRoute] int cityId)
        {
            return Ok(await Mediator.Send(new GetCityDetailByIdQuery { Id = cityId }));
        }

        [HttpGet]
        public async Task<ActionResult<List<CityDetailDto>>> GetCities(
            [FromQuery] int skip,
            [FromQuery] int top)
        {
            return Ok(await Mediator.Send(new GetCitiesListQuery {
                Skip = skip,
                Top = top
            }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateCity(
            [FromBody] CreateCityDto createCity)
        {
            await Mediator.Send(new CreateCityCommand { Name = createCity.Name });

            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut("{cityId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateCityById(
            [FromRoute] int cityId,
            [FromBody] UpdateCityDto updateCity)
        {
            await Mediator.Send(new UpdateCityByIdCommand { Id = cityId, Name = updateCity.Name });
            return Ok();
        }

        [HttpDelete("{cityId:int}")]
        public async Task<IActionResult> DeleteCityById(
            [FromRoute] int cityId)
        {
            await Mediator.Send(new DeleteCityByIdCommand { Id = cityId });
            return Ok();
        }
    }
}