﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Cqrs.Commands.Roles.CreateRole;
using Photostudios.Cqrs.Commands.Roles.UpdateRoleById;
using Photostudios.Cqrs.Commands.Roles.UpdateRoleByName;
using Photostudios.Cqrs.Queries.Roles.GetRoleDetailById;
using Photostudios.Cqrs.Queries.Roles.GetRoleDetailByName;
using Photostudios.Cqrs.Queries.Roles.GetRolesList;
using Photostudios.Cqrs.Queries.Roles.Models;
using Photostudios.Dto.Role;
using Photostudios.Web.Controllers.Base;
using Microsoft.AspNetCore.Http;
using MediatR;
using Photostudios.Mapping.Abstractions;

namespace Photostudios.Web.Controllers
{
    [Route("api/roles")]
    [Authorize(Roles = "Admin")]
    public class RolesController : ApiControllerBase
    {
        public RolesController(IMapper mapper, IMediator mediator)
            : base(mapper, mediator)
        {
        }

        /// <summary>
        /// Get Role by Id
        /// </summary>
        /// <param name="roleId">Role guid</param>
        /// <response code="400">Invalid ID supplied</response>
        /// <response code="204">Role not found</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{roleId:guid}")]
        public async Task<ActionResult<RoleDetailDto>> GetRoleById(
            [FromRoute] Guid roleId)
        {
            return Ok(await Mediator.Send(new GetRoleDetailByIdQuery { Id = roleId }));
        }

        [HttpGet]
        [Route("{roleName}")]
        public async Task<ActionResult<RoleDetailDto>> GetRoleByName(
            [FromRoute] string roleName)
        {
            return Ok(await Mediator.Send(new GetRoleDetailByNameQuery { Name = roleName }));
        }

        [HttpGet]
        public async Task<IActionResult> GetRoles(
            [FromQuery] int skip,
            [FromQuery] int top)
        {
            return Ok(await Mediator.Send(new GetRolesListQuery { Skip = skip, Top = top }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(
            [FromBody] CreateRoleDto createRole)
        {
            await Mediator.Send(new CreateRoleCommand { Name = createRole.Name });
            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut]
        [Route("{roleId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateRoleById(
            [FromRoute] Guid roleId,
            [FromBody] UpdateRoleDto updateRole)
        {
            await Mediator.Send(new UpdateRoleByIdCommand { Id = roleId, Name = updateRole.Name });
            return Ok();
        }

        [HttpPut]
        [Route("{roleName}")]
        public async Task<IActionResult> UpdateRoleByName(
            [FromRoute] string roleName,
            [FromBody] UpdateRoleDto updateRole)
        {
            await Mediator.Send(new UpdateRoleByNameCommand { Name = roleName, UpdatedName = updateRole.Name });
            return Ok();
        }
    }
}