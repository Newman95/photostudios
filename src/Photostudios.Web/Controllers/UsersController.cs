﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Cqrs.Commands.Users.CreateUser;
using Photostudios.Cqrs.Commands.Users.UpdateUserById;
using Photostudios.Cqrs.Queries.Users.GetUserByEmail;
using Photostudios.Cqrs.Queries.Users.GetUserById;
using Photostudios.Cqrs.Queries.Users.GetUsersList;
using Photostudios.Cqrs.Queries.Users.Models;
using Photostudios.Dto.User;
using Photostudios.Web.Controllers.Base;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Photostudios.Cqrs.Commands.Users.DeleteUserById;
using Photostudios.Mapping.Abstractions;
using MediatR;

namespace Photostudios.Web.Controllers
{
    [Route("api/users")]
    [Authorize(Roles = "Admin")]
    public class UsersController : ApiControllerBase
    {
        public UsersController(IMapper mapper, IMediator mediator)
            : base(mapper, mediator)
        {
        }

        [HttpGet("{userId:guid}")]
        public async Task<ActionResult<UserDetailDto>> GetUserById(
            [FromRoute] Guid userId)
        {
            return Ok(await Mediator.Send(new GetUserByIdQuery { Id = userId }));
        }

        [HttpGet("{userEmail}")]
        public async Task<ActionResult<UserDetailDto>> GetUserByEmail(
            [EmailAddress] [FromRoute]string userEmail)
        {
            return Ok(await Mediator.Send(new GetUserByEmailQuery { Email = userEmail }));
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDetailDto>>> GetUsers(
            [FromQuery] int skip,
            [FromQuery] int top)
        {
            return Ok(await Mediator.Send(new GetUsersListQuery { Skip = skip, Top = top }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(
            [FromBody] CreateUserDto createUser)
        {
            await Mediator.Send(new CreateUserCommand
            {
                Email = createUser.Email,
                Password = createUser.Password,
                Roles = createUser.Roles.ToList()
            });

            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut("{userId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateUserById(
            [FromRoute] Guid userId,
            [FromBody] UpdateUserDto updateUser)
        {
            await Mediator.Send(new UpdateUserByIdCommand
            {
                Id = userId,
                UserName = updateUser.UserName
            });

            return Ok();
        }

        [HttpDelete("{userId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> DeleteUserById(
            [FromRoute] Guid userId)
        {
            await Mediator.Send(new DeleteUserByIdCommand
            {
                Id = userId
            });

            return Ok();
        }
    }
}