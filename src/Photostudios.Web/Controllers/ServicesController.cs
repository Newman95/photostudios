﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Photostudios.Cqrs.Commands.Services.CreateService;
using Photostudios.Cqrs.Commands.Services.UpdateServiceById;
using Photostudios.Cqrs.Queries.Services.GetServiceDetailById;
using Photostudios.Cqrs.Queries.Services.GetServicesList;
using Photostudios.Cqrs.Queries.Services.Models;
using Photostudios.Dto.ServiceDto;
using Photostudios.Web.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Photostudios.Mapping.Abstractions;
using MediatR;

namespace Photostudios.Web.Controllers
{
    [Route("api/services")]
    [Authorize(Roles = "Admin")]
    public class ServicesController : ApiControllerBase
    {
        public ServicesController(IMapper mapper, IMediator mediator)
            : base(mapper, mediator)
        {
        }

        [HttpGet("{serviceId:int}")]
        public async Task<ActionResult<ServiceDetailDto>> GetServiceById(
            [FromRoute] int serviceId)
        {
            return Ok(await Mediator.Send(new GetServiceDetailByIdQuery { Id = serviceId }));
        }

        [HttpGet]
        public async Task<ActionResult<List<ServiceDetailDto>>> GetServices(
            [FromQuery] int skip,
            [FromQuery] int top)
        {
            return Ok(await Mediator.Send(new GetServicesListQuery { Skip = skip, Top = top }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateService(
            [FromBody] CreateServiceDto createService)
        {
            await Mediator.Send(new CreateServiceCommand { Name = createService.Name });
            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut("{serviceId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateServiceById(
            [FromRoute] int serviceId,
            [FromBody] UpdateServiceDto updateService)
        {
            await Mediator.Send(new UpdateServiceByIdCommand { Id = serviceId, Name = updateService.Name });
            return Ok();
        }
    }
}