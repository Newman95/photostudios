﻿using AutoMapper;
using Photostudios.Domain.Identity;
using Photostudios.Dto.User;

namespace Photostudios.Mapping.MappingProfiles
{
    public class UserProlife : Profile
    {
        public UserProlife()
        {
            CreateMap<CreateUserDto, User>()
                .ForMember(d => d.UserName, opt => opt.MapFrom(o => o.Email));
        }
    }
}