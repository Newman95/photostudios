﻿using AutoMapper;
using Photostudios.Domain.Identity;
using Photostudios.Dto.Role;

namespace Photostudios.Mapping.MappingProfiles
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<CreateRoleDto, Role>();
        }
    }
}