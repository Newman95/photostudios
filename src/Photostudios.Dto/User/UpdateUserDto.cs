﻿namespace Photostudios.Dto.User
{
    public class UpdateUserDto
    {
        public string UserName { get; set; }
    }
}