﻿using System;
using System.Collections.Generic;

namespace Photostudios.Dto.User
{
    public class CreateUserDto
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string RepeatPassword { get; set; }

        public ICollection<string> Roles { get; set; } = Array.Empty<string>();
    }
}