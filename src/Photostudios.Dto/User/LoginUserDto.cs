using System.ComponentModel.DataAnnotations;

namespace Photostudios.Dto.User
{
    public class LoginUserDto
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}