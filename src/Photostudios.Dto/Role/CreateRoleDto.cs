﻿namespace Photostudios.Dto.Role
{
    public class CreateRoleDto
    {
        public string Name { get; set; }
    }
}
