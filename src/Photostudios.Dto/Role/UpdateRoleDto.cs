﻿namespace Photostudios.Dto.Role
{
    public class UpdateRoleDto
    {
        public string Name { get; set; }
    }
}