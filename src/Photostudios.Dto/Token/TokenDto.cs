﻿namespace Photostudios.Dto.Token
{
    public class TokenDto
    {
        public string Token { get; set; }
    }
}
