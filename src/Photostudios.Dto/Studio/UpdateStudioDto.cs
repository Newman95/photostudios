﻿using System;
using System.Collections.Generic;

namespace Photostudios.Dto.Studio
{
    public class UpdateStudioDto
    {
        public UpdateStudioDto()
        {
            Phones = new HashSet<string>();
            Photos = new HashSet<int>();
            Services = new HashSet<int>();
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public int LogoId { get; set; }

        public Guid OwnerId { get; set; }

        public int CityId { get; set; }

        public ICollection<string> Phones { get; }

        public ICollection<int> Photos { get; }

        public ICollection<int> Services { get; }
    }
}