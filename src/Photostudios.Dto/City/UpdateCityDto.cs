﻿namespace Photostudios.Dto.City
{
    public class UpdateCityDto
    {
        public string Name { get; set; }
    }
}