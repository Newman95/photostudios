﻿namespace Photostudios.Dto.City
{
    public class CreateCityDto
    {
        public string Name { get; set; }
    }
}