﻿using FluentValidation;

namespace Photostudios.Dto.City
{
    public class CreateCityDtoValidator : AbstractValidator<CreateCityDto>
    {
        public CreateCityDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
