﻿namespace Photostudios.Configuration
{
    public static class ConfigurationPaths
    {
        public const string DbMain = "Databases:Main";
        public const string Token = "Token";
    }
}