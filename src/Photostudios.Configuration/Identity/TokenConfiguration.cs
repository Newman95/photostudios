using Photostudios.Configuration.Abstractions;

namespace Photostudios.Configuration.Identity
{
    public class TokenConfiguration : ITokenConfiguration
    {
        public string Key { get; set; }

        public string Audience { get; set; }
    }
}