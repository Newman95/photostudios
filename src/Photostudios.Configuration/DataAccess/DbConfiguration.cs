using Photostudios.Configuration.Abstractions;

namespace Photostudios.Configuration.DataAccess
{
    public class DbConfiguration : IDbConfiguration
    {
        public string ConnectionString { get; set; }

        public int CommandTimeout { get; set; }

        public int RetryCount { get; set; }
    }
}