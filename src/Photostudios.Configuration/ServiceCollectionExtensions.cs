﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Photostudios.Configuration.DataAccess;
using Photostudios.Configuration.Identity;
using System;

namespace Photostudios.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddConfiguration(
            this IServiceCollection services,
            IConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            services
                .AddOptions()
                .Configure<TokenConfiguration>(config.GetSection(ConfigurationPaths.Token))
                .Configure<DbConfiguration>(config.GetSection(ConfigurationPaths.DbMain));

            return services;
        }
    }
}