﻿using Autofac;
using Microsoft.Extensions.Options;
using Photostudios.Configuration.Abstractions;
using Photostudios.Configuration.DataAccess;
using Photostudios.Configuration.Identity;

namespace Photostudios.Configuration.Di
{
    public class DependenciesContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .Register<IDbConfiguration>(c => c.Resolve<IOptionsSnapshot<DbConfiguration>>().Value)
                .SingleInstance();

            builder
                .Register<ITokenConfiguration>(c => c.Resolve<IOptionsSnapshot<TokenConfiguration>>().Value)
                .SingleInstance();

            base.Load(builder);
        }
    }
}