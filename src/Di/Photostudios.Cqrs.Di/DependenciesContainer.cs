﻿using Autofac;
using MediatR;
using Photostudios.Cqrs.Behaviors;
using Photostudios.Cqrs.Commands.Cities.CreateCity;
using Photostudios.Cqrs.Queries.Cities.GetCitiesList;

namespace Photostudios.Cqrs.Di
{
    public class DependenciesContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(LoggingBehavior<,>))
                   .As(typeof(IPipelineBehavior<,>));

            builder.RegisterGeneric(typeof(PerformanceBehaviour<,>))
                   .As(typeof(IPipelineBehavior<,>));

            builder.RegisterAssemblyTypes(typeof(CreateCityCommand).Assembly)
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(GetCitiesListQuery).Assembly)
                   .AsImplementedInterfaces();

            base.Load(builder);
        }
    }
}
