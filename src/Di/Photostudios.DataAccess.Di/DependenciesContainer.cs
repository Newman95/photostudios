﻿using Autofac;
using Photostudios.DataAccess.EF;

namespace Photostudios.DataAccess.Di
{
    public class DependenciesContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AppDbContext>().AsSelf();

            base.Load(builder);
        }
    }
}