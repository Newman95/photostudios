namespace Photostudios.Configuration.Abstractions
{
    public interface ITokenConfiguration
    {
        string Key { get; set; }

        string Audience { get; set; }
    }
}