using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Roles.UpdateRoleByName
{
    public class UpdateRoleByNameCommandHandler : AsyncRequestHandler<UpdateRoleByNameCommand>
    {
        private readonly RoleManager<Role> roleManager;

        public UpdateRoleByNameCommandHandler(
            RoleManager<Role> roleManager)
        {
            this.roleManager = roleManager;
        }

        protected override async Task Handle(UpdateRoleByNameCommand request, CancellationToken cancellationToken)
        {
            var role = await roleManager
                .FindByNameAsync(request.Name.ToString(CultureInfo.InvariantCulture))
                ;

            if (role == null)
            {
                throw new InvalidOperationException($"There is no role with name {request.Name}");
            }

            await roleManager.SetRoleNameAsync(role, request.UpdatedName);
        }
    }
}
