using MediatR;

namespace Photostudios.Cqrs.Commands.Roles.UpdateRoleByName
{
    public class UpdateRoleByNameCommand : IRequest
    {
        public string Name { get; set; }

        public string UpdatedName { get; set; }
    }
}
