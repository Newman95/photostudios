using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Roles.CreateRole
{
    public class CreateRoleCommandHandler : AsyncRequestHandler<CreateRoleCommand>
    {
        private readonly RoleManager<Role> roleManager;

        public CreateRoleCommandHandler(
            RoleManager<Role> roleManager)
        {
            this.roleManager = roleManager;
        }

        protected override Task Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            var role = new Role { Name = request.Name };
            return roleManager.CreateAsync(role);
        }
    }
}
