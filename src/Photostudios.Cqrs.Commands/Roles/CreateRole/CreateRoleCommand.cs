using MediatR;

namespace Photostudios.Cqrs.Commands.Roles.CreateRole
{
    public class CreateRoleCommand : IRequest
    {
        public string Name { get; set; }
    }
}
