using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Roles.UpdateRoleById
{
    public class UpdateRoleByIdCommandHandler : AsyncRequestHandler<UpdateRoleByIdCommand>
    {
        private readonly RoleManager<Role> roleManager;

        public UpdateRoleByIdCommandHandler(
            RoleManager<Role> roleManager)
        {
            this.roleManager = roleManager;
        }

        protected override async Task Handle(UpdateRoleByIdCommand request, CancellationToken cancellationToken)
        {
            var role = await roleManager.FindByIdAsync(request.Id.ToString());

            if (role == null)
            {
                throw new InvalidOperationException($"There is no role with id {request.Id}");
            }

            await roleManager.SetRoleNameAsync(role, request.Name);
        }
    }
}
