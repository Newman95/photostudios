using System;
using MediatR;

namespace Photostudios.Cqrs.Commands.Roles.UpdateRoleById
{
    public class UpdateRoleByIdCommand : IRequest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
