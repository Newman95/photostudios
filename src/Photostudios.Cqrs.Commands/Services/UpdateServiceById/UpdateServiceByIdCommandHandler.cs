using MediatR;
using Photostudios.DataAccess.EF;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Photostudios.Cqrs.Commands.Services.UpdateServiceById
{
    public class UpdateServiceByIdCommandHandler : AsyncRequestHandler<UpdateServiceByIdCommand>
    {
        private readonly AppDbContext _context;

        public UpdateServiceByIdCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(UpdateServiceByIdCommand request, CancellationToken cancellationToken)
        {
            var service = await _context.Services.FindAsync(request.Id);

            if (service == null)
            {
                throw new InvalidOperationException($"There is no service with id {request.Id}");
            }

            service.Name = request.Name;

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
