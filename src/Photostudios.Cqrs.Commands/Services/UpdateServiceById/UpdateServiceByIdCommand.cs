using MediatR;

namespace Photostudios.Cqrs.Commands.Services.UpdateServiceById
{
    public class UpdateServiceByIdCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
