using MediatR;
using Photostudios.DataAccess.EF;
using Photostudios.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace Photostudios.Cqrs.Commands.Services.CreateService
{
    public class CreateServiceCommandHandler : AsyncRequestHandler<CreateServiceCommand>
    {
        private readonly AppDbContext _context;

        public CreateServiceCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(CreateServiceCommand request, CancellationToken cancellationToken)
        {
            var service = new Service { Name = request.Name };

            await _context.Services.AddAsync(service, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
