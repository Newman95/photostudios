using MediatR;

namespace Photostudios.Cqrs.Commands.Services.CreateService
{
    public class CreateServiceCommand : IRequest
    {
        public string Name { get; set; }
    }
}
