using MediatR;

namespace Photostudios.Cqrs.Commands.Studios.UpdateStudioById
{
    public class UpdateStudioByIdCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}
