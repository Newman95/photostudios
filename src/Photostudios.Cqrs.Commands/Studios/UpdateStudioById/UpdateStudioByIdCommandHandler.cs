using MediatR;
using Photostudios.DataAccess.EF;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Photostudios.Cqrs.Commands.Studios.UpdateStudioById
{
    public class UpdateStudioByIdCommandHandler : AsyncRequestHandler<UpdateStudioByIdCommand>
    {
        private readonly AppDbContext _context;

        public UpdateStudioByIdCommandHandler(AppDbContext context)
        {
            _context = context;
        }
        protected override async Task Handle(UpdateStudioByIdCommand request, CancellationToken cancellationToken)
        {
            var studio = await _context.Studios.FindAsync(request.Id);

            if (studio == null)
            {
                throw new InvalidOperationException($"There is no studio with id {request.Id}");
            }

            studio.Name = request.Name;
            studio.Address = request.Address;

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
