using MediatR;
using System;
using System.Collections.Generic;

namespace Photostudios.Cqrs.Commands.Studios.CreateStudio
{
    public class CreateStudioCommand : IRequest
    {
        public CreateStudioCommand()
        {
            Phones = new HashSet<string>();
            Photos = new HashSet<int>();
            Services = new HashSet<int>();
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public int LogoId { get; set; }

        public Guid OwnerId { get; set; }

        public int CityId { get; set; }

        public ICollection<string> Phones { get; set; }

        public ICollection<int> Photos { get; set; }

        public ICollection<int> Services { get; set; }
    }
}
