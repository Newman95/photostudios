using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.DataAccess.EF;
using Photostudios.Domain;

namespace Photostudios.Cqrs.Commands.Studios.CreateStudio
{
    public class CreateStudioCommandHandler : AsyncRequestHandler<CreateStudioCommand>
    {
        private readonly AppDbContext _context;

        public CreateStudioCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(CreateStudioCommand request, CancellationToken cancellationToken)
        {
            var studio = new Studio
            {
                Name = request.Name,
                Address = request.Address,
                LogoId = request.LogoId,
                OwnerId = request.OwnerId,
                CityId = request.CityId
            };

            foreach (var phone in request.Phones)
            {
                studio.Phones.Add(new Phone { Number = phone });
            }

            foreach (var photoId in request.Photos)
            {
                studio.Photos.Add(new Asset { Id = photoId });
            }

            foreach (var serviceId in request.Services)
            {
                studio.StudioServices.Add(new StudioService { ServiceId = serviceId });
            }

            await _context.Studios.AddAsync(studio, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
