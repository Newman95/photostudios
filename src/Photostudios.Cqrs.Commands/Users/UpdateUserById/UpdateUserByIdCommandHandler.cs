using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Users.UpdateUserById
{
    public class UpdateUserByIdCommandHandler : AsyncRequestHandler<UpdateUserByIdCommand>
    {
        private readonly UserManager<User> userManager;

        public UpdateUserByIdCommandHandler(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        protected override async Task Handle(UpdateUserByIdCommand request, CancellationToken cancellationToken)
        {
            var user = await userManager.FindByIdAsync(request.Id.ToString());

            if (user == null)
            {
                throw new InvalidOperationException($"There is no user with id ${request.Id}");
            }

            await userManager.SetUserNameAsync(user, request.UserName);
        }
    }
}
