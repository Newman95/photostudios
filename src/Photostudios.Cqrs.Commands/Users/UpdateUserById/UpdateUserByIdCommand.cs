using System;
using MediatR;

namespace Photostudios.Cqrs.Commands.Users.UpdateUserById
{
    public class UpdateUserByIdCommand : IRequest
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }
    }
}
