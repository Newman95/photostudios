using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Users.CreateUser
{
    public class CreateUserCommandHandler : AsyncRequestHandler<CreateUserCommand>
    {
        private readonly UserManager<User> userManager;

        public CreateUserCommandHandler(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        protected override async Task Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = new User
            {
                Email = request.Email,
                UserName = request.Email
            };

            var identityResult = await userManager.CreateAsync(user, request.Password);

            if (identityResult.Succeeded && request.Roles.Any())
            {
                user = await userManager.FindByEmailAsync(request.Email);

                foreach (var role in request.Roles)
                {
                    await userManager.AddToRoleAsync(user, role);
                }
            }
        }
    }
}
