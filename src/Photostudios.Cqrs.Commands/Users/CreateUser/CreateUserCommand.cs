using System;
using System.Collections.Generic;
using MediatR;

namespace Photostudios.Cqrs.Commands.Users.CreateUser
{
    public class CreateUserCommand : IRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public IReadOnlyList<string> Roles { get; set; } = Array.Empty<string>();
    }
}
