﻿using System;
using MediatR;

namespace Photostudios.Cqrs.Commands.Users.DeleteUserById
{
    public class DeleteUserByIdCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
