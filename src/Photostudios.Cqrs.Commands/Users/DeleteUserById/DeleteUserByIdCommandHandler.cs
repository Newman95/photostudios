﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;

namespace Photostudios.Cqrs.Commands.Users.DeleteUserById
{
    public class DeleteUserByIdCommandHandler : AsyncRequestHandler<DeleteUserByIdCommand>
    {
        private readonly UserManager<User> userManager;

        public DeleteUserByIdCommandHandler(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        protected override async Task Handle(DeleteUserByIdCommand request, CancellationToken cancellationToken)
        {
            var user = await userManager.FindByIdAsync(request.Id.ToString());

            if (user != null)
            {
                await userManager.DeleteAsync(user);
            }
        }
    }
}
