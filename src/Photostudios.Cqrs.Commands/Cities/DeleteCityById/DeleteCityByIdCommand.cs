using MediatR;

namespace Photostudios.Cqrs.Commands.Cities.DeleteCityById
{
    public class DeleteCityByIdCommand : IRequest
    {
        public int Id { get; set; }
    }
}