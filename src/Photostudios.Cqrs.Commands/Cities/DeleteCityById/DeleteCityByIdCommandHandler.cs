using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Commands.Cities.DeleteCityById
{
    public class DeleteCityByIdCommandHandler : AsyncRequestHandler<DeleteCityByIdCommand>
    {
        private readonly AppDbContext _context;

        public DeleteCityByIdCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(DeleteCityByIdCommand request, CancellationToken cancellationToken)
        {
            var city = await _context.Cities.FindAsync(request.Id);

            if (city == null)
            {
                throw new InvalidOperationException($"There is no city with id {request.Id}");
            }

            _context.Cities.Remove(city);
            await _context.SaveChangesAsync();
        }
    }
}
