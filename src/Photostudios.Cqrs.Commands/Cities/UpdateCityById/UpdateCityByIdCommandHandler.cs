using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.DataAccess.EF;

namespace Photostudios.Cqrs.Commands.Cities.UpdateCityById
{
    public class UpdateCityByIdCommandHandler : AsyncRequestHandler<UpdateCityByIdCommand>
    {
        private readonly AppDbContext _context;

        public UpdateCityByIdCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(UpdateCityByIdCommand request, CancellationToken cancellationToken)
        {
            var city = await _context.Cities.FindAsync(request.Id);

            if (city == null)
            {
                throw new InvalidOperationException($"There is no city with id {request.Id}");
            }

            city.Name = request.Name;

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
