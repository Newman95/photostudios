using MediatR;

namespace Photostudios.Cqrs.Commands.Cities.UpdateCityById
{
    public class UpdateCityByIdCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
