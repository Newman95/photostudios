using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Photostudios.DataAccess.EF;
using Photostudios.Domain;

namespace Photostudios.Cqrs.Commands.Cities.CreateCity
{
    public class CreateCityCommandHandler : AsyncRequestHandler<CreateCityCommand>
    {
        private readonly AppDbContext _context;

        public CreateCityCommandHandler(AppDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(CreateCityCommand request, CancellationToken cancellationToken)
        {
            var city = new City { Name = request.Name };

            await _context.Cities.AddAsync(city, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
