using MediatR;

namespace Photostudios.Cqrs.Commands.Cities.CreateCity
{
    public class CreateCityCommand : IRequest
    {
        public string Name { get; set; }
    }
}
