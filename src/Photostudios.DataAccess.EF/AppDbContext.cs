using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Photostudios.Configuration.Abstractions;
using Photostudios.DataAccess.EF.Seeders;
using Photostudios.Domain.Identity;
using Photostudios.Web.Extensions;

namespace Photostudios.DataAccess.EF
{
    public partial class AppDbContext
        : IdentityDbContext<User, Role, Guid, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        private readonly IDbConfiguration configuration;

        public AppDbContext(IDbConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                base.OnConfiguring(optionsBuilder);
            }

            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll);

            optionsBuilder.UseSqlite(configuration.ConnectionString);

            //optionsBuilder.UseSqlServer(configuration.ConnectionString, builder =>
            //{
            //    builder.CommandTimeout(configuration.CommandTimeout);
            //    builder.EnableRetryOnFailure(configuration.RetryCount);
            //});
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);

            builder.ApplySeed(new CitySeed())
                   .ApplySeed(new ServiceSeed());
        }
    }
}