﻿using Microsoft.EntityFrameworkCore;
using Photostudios.DataAccess.EF.Seeders.Abstractions;

namespace Photostudios.Web.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ApplySeed<TEntity>(
            this ModelBuilder builder,
            IEntityTypeSeed<TEntity> seeder) where TEntity: class
        {
            if (seeder != null)
            {
                seeder.Seed(builder.Entity<TEntity>());
            }

            return builder;
        }
    }
}
