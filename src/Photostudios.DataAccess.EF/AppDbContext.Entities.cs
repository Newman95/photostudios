﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Photostudios.Domain;
using Photostudios.Domain.Identity;
using System;

namespace Photostudios.DataAccess.EF
{
    public partial class AppDbContext
        : IdentityDbContext<User, Role, Guid, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Studio> Studios { get; set; }
        public virtual DbSet<StudioService> StudioServices { get; set; }
    }
}