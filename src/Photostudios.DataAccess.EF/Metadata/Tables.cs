﻿namespace Photostudios.DataAccess.EF.Metadata
{
    internal static class Tables
    {
        public const string Assets = nameof(Assets);
        public const string Cities = nameof(Cities);
        public const string Phones = nameof(Phones);
        public const string RoleClaims = nameof(RoleClaims);
        public const string Roles = nameof(Roles);
        public const string Services = nameof(Services);
        public const string Studios = nameof(Studios);
        public const string StudioServices = nameof(StudioServices);
        public const string UserClaims = nameof(UserClaims);
        public const string UserLogins = nameof(UserLogins);
        public const string UserRoles = nameof(UserRoles);
        public const string Users = nameof(Users);
        public const string UserTokens = nameof(UserTokens);
    }
}