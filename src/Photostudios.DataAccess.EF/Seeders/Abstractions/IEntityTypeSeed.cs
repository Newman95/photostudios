using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Photostudios.DataAccess.EF.Seeders.Abstractions
{
    public interface IEntityTypeSeed<TEntity> where TEntity: class
    {
        void Seed(EntityTypeBuilder<TEntity> builder);
    }
}
