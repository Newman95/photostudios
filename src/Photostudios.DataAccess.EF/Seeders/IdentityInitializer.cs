﻿using Microsoft.AspNetCore.Identity;
using Photostudios.Domain.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photostudios.DataAccess.EF.Seeders
{
    public static class IdentityInitializer
    {
        private const string DefaultPass = "QazWsx123";

        public static Task InitializeAsync(
            AppDbContext context,
            UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (userManager == null)
            {
                throw new ArgumentNullException(nameof(userManager));
            }

            if (roleManager == null)
            {
                throw new ArgumentNullException(nameof(roleManager));
            }

            return InitializeInternalAsync(context, userManager, roleManager);
        }

        private static async Task InitializeInternalAsync(
            AppDbContext context,
            UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {
            await context.Database.EnsureCreatedAsync();

            if (context.Users.Any())
            {
                return;
            }

            await CreateDefaultRoles(roleManager, DefaultRoles());
            await CreateDefaultUsers(userManager, DefaultUsers());
        }

        private static async Task CreateDefaultRoles(
            RoleManager<Role> roleManager,
            IEnumerable<string> roles)
        {
            foreach (var role in roles)
            {
                await roleManager.CreateAsync(new Role(role));
            }
        }

        private static async Task CreateDefaultUsers(
            UserManager<User> userManager,
            IDictionary<string, string> users)
        {
            foreach (var user in users)
            {
                var role = user.Key;
                var email = user.Value;

                var newUser = new User { UserName = email, Email = email };

                await userManager.CreateAsync(newUser);
                var createdUser = await userManager.FindByEmailAsync(email);
                await userManager.AddPasswordAsync(createdUser, DefaultPass);
                await userManager.AddToRoleAsync(createdUser, role);
            }
        }

        private static IEnumerable<string> DefaultRoles()
        {
            return new[] { "Admin", "Customer" };
        }

        private static IDictionary<string, string> DefaultUsers()
        {
            return new Dictionary<string, string>
            {
                ["Admin"] = "admin@mail.ua",
                ["Customer"] = "customer@mail.ua"
            };
        }
    }
}