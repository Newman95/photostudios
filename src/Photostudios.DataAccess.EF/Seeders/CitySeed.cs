using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Seeders.Abstractions;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Seeders
{
    public class CitySeed : IEntityTypeSeed<City>
    {
        public void Seed(EntityTypeBuilder<City> builder)
        {
            builder.HasData(
                new City { Id = 1, Name = "Lviv" }
            );
        }
    }
}