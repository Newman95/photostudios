using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Seeders.Abstractions;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Seeders
{
    public class ServiceSeed : IEntityTypeSeed<Service>
    {
        public void Seed(EntityTypeBuilder<Service> builder)
        {
            builder.HasData(
                new Service { Id = 1, Name = "Оренда" }
            );
        }
    }
}