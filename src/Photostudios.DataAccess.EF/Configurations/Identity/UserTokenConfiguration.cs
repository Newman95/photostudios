using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain.Identity;

namespace Photostudios.DataAccess.EF.Configurations
{
    public class UserTokenConfiguration : IEntityTypeConfiguration<UserToken>
    {
        public void Configure(EntityTypeBuilder<UserToken> builder)
        {
            builder.ToTable(Tables.UserTokens, Schemas.Dbo);
        }
    }
}