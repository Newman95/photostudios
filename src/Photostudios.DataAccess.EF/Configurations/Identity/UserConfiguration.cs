using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain.Identity;

namespace Photostudios.DataAccess.EF.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(Tables.Users, Schemas.Dbo);

            builder.HasOne(u => u.Studio)
                   .WithOne(s => s.Owner)
                   .HasForeignKey<User>(u => u.StudioId);

            builder.HasMany(u => u.Claims)
                   .WithOne(c => c.User)
                   .HasForeignKey(uc => uc.UserId)
                   .IsRequired();

            builder.HasMany(e => e.Logins)
                   .WithOne(l => l.User)
                   .HasForeignKey(ul => ul.UserId)
                   .IsRequired();

            builder.HasMany(e => e.Tokens)
                   .WithOne(t => t.User)
                   .HasForeignKey(ut => ut.UserId)
                   .IsRequired();

            builder.HasMany(e => e.UserRoles)
                   .WithOne(ur => ur.User)
                   .HasForeignKey(ur => ur.UserId)
                   .IsRequired();

        }
    }
}