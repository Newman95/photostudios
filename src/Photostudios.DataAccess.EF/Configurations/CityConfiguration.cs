using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Configurations
{
    internal class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.ToTable(Tables.Cities, Schemas.Dbo);

            builder.HasKey(c => c.Id);

            builder.HasAlternateKey(c => c.Name);

            builder.Property(c => c.Name)
                   .IsRequired()
                   .HasMaxLength(32);

            builder.HasMany(c => c.Studios)
                   .WithOne(s => s.City)
                   .HasForeignKey(s => s.CityId);
        }
    }
}