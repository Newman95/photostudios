using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Configurations
{
    public class StudioConfiguration : IEntityTypeConfiguration<Studio>
    {
        public void Configure(EntityTypeBuilder<Studio> builder)
        {
            builder.ToTable(Tables.Studios, Schemas.Dbo);

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name)
                   .IsRequired()
                   .HasMaxLength(128);

            builder.HasIndex(s => s.Name)
                   .IsUnique();

            builder.Property(s => s.Address)
                   .IsRequired()
                   .HasMaxLength(128);

            builder.HasOne(s => s.Logo)
                   .WithMany(a => a.Studios)
                   .HasForeignKey(s => s.LogoId);

            builder.HasOne(s => s.Owner)
                   .WithOne(u => u.Studio)
                   .HasForeignKey<Studio>(s => s.OwnerId);

            builder.HasOne(s => s.City)
                   .WithMany(c => c.Studios)
                   .HasForeignKey(s => s.CityId);

            builder.HasMany(s => s.Phones)
                   .WithOne(p => p.Studio)
                   .HasForeignKey(p => p.StudioId);

            builder.HasMany(s => s.Photos);
        }
    }
}