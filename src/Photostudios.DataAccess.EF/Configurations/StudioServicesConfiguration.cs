using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Configurations
{
    internal class StudioServicesConfiguration : IEntityTypeConfiguration<StudioService>
    {
        public void Configure(EntityTypeBuilder<StudioService> builder)
        {
            builder.ToTable(Tables.StudioServices, Schemas.Dbo);

            builder.HasKey(ss => new { ss.StudioId, ss.ServiceId });

            builder.HasOne(ss => ss.Studio)
                   .WithMany(s => s.StudioServices)
                   .HasForeignKey(ss => ss.StudioId);

            builder.HasOne(ss => ss.Service)
                   .WithMany(s => s.StudioServices)
                   .HasForeignKey(ss => ss.ServiceId);
        }
    }
}