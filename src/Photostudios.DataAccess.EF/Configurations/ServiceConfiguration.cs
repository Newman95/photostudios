using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Configurations
{
    internal class ServiceConfiguration : IEntityTypeConfiguration<Service>
    {
        public void Configure(EntityTypeBuilder<Service> builder)
        {
            builder.ToTable(Tables.Services, Schemas.Dbo);

            builder.HasKey(s => s.Id);

            builder.HasAlternateKey(s => s.Name);

            builder.Property(s => s.Name)
                   .IsRequired()
                   .HasMaxLength(64);
        }
    }
}