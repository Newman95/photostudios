using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Photostudios.DataAccess.EF.Metadata;
using Photostudios.Domain;

namespace Photostudios.DataAccess.EF.Configurations
{
    internal class PhoneConfiguration : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.ToTable(Tables.Phones, Schemas.Dbo);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Number)
                   .IsRequired()
                   .HasMaxLength(16);

            builder.HasOne(p => p.Studio)
                   .WithMany(s => s.Phones)
                   .HasForeignKey(p => p.StudioId);
        }
    }
}