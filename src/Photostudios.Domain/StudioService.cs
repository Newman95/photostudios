namespace Photostudios.Domain
{
    public class StudioService
    {
        public int StudioId { get; set; }

        public Studio Studio { get; set; }

        public int ServiceId { get; set; }

        public Service Service { get; set; }
    }
}
