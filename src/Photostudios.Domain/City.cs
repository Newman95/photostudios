using Photostudios.DataAccess.Abstractions.Domain;
using System.Collections.Generic;

namespace Photostudios.Domain
{
    public class City : EntityBase<int>
    {
        public string Name { get; set; }

        public ICollection<Studio> Studios { get; set; }
    }
}