using Photostudios.DataAccess.Abstractions.Domain;

namespace Photostudios.Domain
{
    public class Phone : EntityBase<int>
    {
        public string Number { get; set; }

        public int StudioId { get; set; }

        public Studio Studio { get; set; }
    }
}