using Photostudios.DataAccess.Abstractions.Domain;
using Photostudios.Domain.Identity;
using System;
using System.Collections.Generic;

namespace Photostudios.Domain
{
    public class Studio : EntityBase<int>
    {
        public Studio()
        {
            Phones = new HashSet<Phone>();
            Photos = new HashSet<Asset>();
            StudioServices = new HashSet<StudioService>();
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public int LogoId { get; set; }

        public Asset Logo { get; set; }

        public Guid OwnerId { get; set; }

        public User Owner { get; set; }

        public int CityId { get; set; }

        public City City { get; set; }

        public ICollection<Phone> Phones { get; set; }

        public ICollection<Asset> Photos { get; set; }

        public ICollection<StudioService> StudioServices { get; set; }
    }
}