using Photostudios.DataAccess.Abstractions.Domain;
using System;
using System.Collections.Generic;

namespace Photostudios.Domain
{
    public class Asset : EntityBase<int>
    {
        public string Name { get; set; }

        public Uri Url { get; set; }

        public ICollection<Studio> Studios { get; set; }
    }
}