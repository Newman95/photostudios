﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Photostudios.Domain.Identity
{
    public class RoleClaim : IdentityRoleClaim<Guid>
    {
        public Role Role { get; set; }
    }
}
