﻿using Microsoft.AspNetCore.Identity;
using Photostudios.DataAccess.Abstractions;
using System;

namespace Photostudios.Domain.Identity
{
    public class UserToken : IdentityUserToken<Guid>
    {
        public User User { get; set; }
    }
}
