﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Photostudios.Domain.Identity
{
    public class UserClaim : IdentityUserClaim<Guid>
    {
        public User User { get; set; }
    }
}