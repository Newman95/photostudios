using Microsoft.AspNetCore.Identity;
using Photostudios.DataAccess.Abstractions;
using System;
using System.Collections.Generic;

namespace Photostudios.Domain.Identity
{
    public class User : IdentityUser<Guid>, IEntity<Guid>
    {
        public User()
            : base()
        {
        }

        public User(string userName)
            : base(userName)
        {
        }

        public int? StudioId { get; set; }
        public Studio Studio { get; set; }

        public ICollection<UserClaim> Claims { get; set; }
        public ICollection<UserLogin> Logins { get; set; }
        public ICollection<UserToken> Tokens { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}