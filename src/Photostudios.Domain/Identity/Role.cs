using Microsoft.AspNetCore.Identity;
using Photostudios.DataAccess.Abstractions;
using System;
using System.Collections.Generic;

namespace Photostudios.Domain.Identity
{
    public class Role : IdentityRole<Guid>, IEntity<Guid>
    {
        public Role() : base()
        {
        }

        public Role(string roleName):base(roleName)
        {
        }

        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<RoleClaim> RoleClaims { get; set; }
    }
}