﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Photostudios.Domain.Identity
{
    public class UserLogin : IdentityUserLogin<Guid>
    {
        public User User { get; set; }
    }
}